extern crate lib_texdown ;
extern crate clap;
pub mod stack;
use std::str::CharIndices;
use clap::{ Arg, App } ;
use std::io::Result as IoRes ;
use std::io::Write;
use lib_texdown::ast::{ SmplTxt, RichTxt, Frame, Frames } ;
use std::fs::File;
use std::io;
//mod stack;
/// Create the presentation.
fn generate_presentation(string : &str) -> Frames {
  match Frames::of_file(string, false) {
    Ok(frames) => frames,
    Err(e) => panic!(
      "Error opening file :(."
    ),
  }
}
/// Markers for simple text.
pub static BOLD_TXT: &'static str = "\\textbf";
pub static ITAL_TXT: &'static str = "\\textit";
pub static HREF: &'static str = "\\href";
pub static MONOSPACE: &'static str = "\\texttt";
/// Markers for rich text.
pub static LISTING: &'static str = "{lstlisting}";
pub static BEGIN: &'static str = "\\begin";
pub static END: &'static str = "\\end";
pub static VERBATIM: &'static str = "{verbatim}";
pub static FRAGILE : &'static str = "[fragile]";
pub static DISP_QUOTE: &'static str = "{displayquote}";
pub static ITEM: &'static str = "\\item";
pub static CHAR_SPACE: &'static str = "";
pub static LISTSET: &'static str = "{lstset}";
pub static NUMBER: char = '#' ;
/// To latex translation.
fn char_option(opt : Option<(usize, char)> ) -> char {
    match opt {
      Some(x) => x.1 ,
      None => ' ' ,
    }
}
pub trait ToLatex {
  /// Translates something to latex, writes it in the writer.
  fn to_latex<Writer: Write>(& self, w: & mut Writer, blah : &str, boo: bool) -> IoRes<()> ;
}
pub fn duplicate_backtrace(locations_to_exclude : & Vec<usize>, shift : & usize, offset : usize) -> bool {
  let mut tempnum : usize = 0;
  if locations_to_exclude.len() > 0 {
    tempnum = locations_to_exclude[locations_to_exclude.len() - 1];
  }
  println!("{} < - tempnum", tempnum);
  if tempnum != 0 && tempnum == *shift + offset {
    true
  }
  else {
    false
  }
}
pub fn option_str(opt : Option<String>) -> String {
  match opt {
    Some(x) => x,
    None => "".to_string(),
  }
}
pub fn option_usize(opt : Option<usize>) -> usize {
  match opt {
    Some(x) => x,
    None => 0,
  }
}
pub fn lst_input<Writer: Write>(w : & mut Writer, txt : & String, shift : & usize) -> (String, usize) {
  let mut chr_index : CharIndices = txt.char_indices();
  let mut temp_car = ' ' ;
  let mut s : String = String::new();
  let mut count = 0usize;
  while count <= *shift + 2 {
    char_option(chr_index.next());
    count += 1;
  }
  count = 0;
  let mut tempstring = String::new();
  temp_car = char_option(chr_index.next());
  while temp_car != '>'  {
    tempstring.push(temp_car);
    count += 1;
    temp_car = char_option(chr_index.next());
  }
  (tempstring, count)
}
impl ToLatex for SmplTxt {
  #[allow(unused_imports)]
  fn to_latex<Writer: Write>(& self, w: & mut Writer, blah : &str, boo : bool) -> IoRes<()> {
    // Bring all variants of `SmplTxt` in scope.
    //mod stack;
    use stack::*;
    use lib_texdown::ast::SmplTxt::* ;
    use lib_texdown::ast::{Frame};
    match * self {
      SmplTxt::Seq(ref vec) => {
        for smpl_txt in vec.iter() {
          // Could stack overflow, but the depth of the AST is unlikely to be
          // high enough.
          //println!("@ SmplTxt::seq");
          try!( smpl_txt.to_latex(w, "", false) ) ;
          try!( w.flush() )
        } ;
        Ok(())
      },
      SmplTxt::Txt(ref txt) => {
        let mut string : String = String::new();
        let mut vectxt : CharIndices = txt.char_indices();
        let mut size_of_txt = txt.len();
        let mut shift : usize = 0;
        let mut zero  = ' ' ;
        let mut one = ' ' ;
        let mut two = ' ' ;
        let mut three = ' ' ;
        let mut four = ' ' ;
        let mut five = ' ' ;
        let mut difference : i32 = 0;
        zero = char_option(vectxt.next());
        one = char_option(vectxt.next());
        two = char_option(vectxt.next());
        three = char_option(vectxt.next());
        four = char_option(vectxt.next());
        five = char_option(vectxt.next());
        //println!("{}{}{}{}{}{}", zero , one  , two , three , four , five);
        while shift < txt.len()  {
          difference = 0;
          if zero  == '<'  && two == '=' {
            match one as char {
              'j' => {
                //println!("matched!");
                let (tempy, exclude_this) = lst_input(w, txt, &shift);
                let s_stuff : String = format!("\\lstinputlisting[language=Java, style=custom]{{{}}}", tempy);
                string = format!("{0}{1}", s_stuff, string);
                difference = tempy.len() as i32 + 4;
              },
              'p' => {
                //println!("matched!");
                let (tempy, exclude_this) = lst_input(w, txt, &shift);
                let s_stuff : String = format!("\\lstinputlisting[language=Python, style=custom]{{{}}}", tempy);
                string = format!("{0}{1}", s_stuff, string);
                difference = tempy.len() as i32  + 4;
              },
              'r' => {
                //println!("matched!");
                let (tempy, exclude_this) = lst_input(w, txt, &shift);
                let s_stuff : String = format!("\\lstinputlisting[language=Rust, style=custom]{{{}}}", tempy);
                string = format!("{0}{1}", s_stuff, string);
                difference = tempy.len() as i32  + 4;
              },
              'h' => {
                //println!("matched!");
                let (tempy, exclude_this) = lst_input(w, txt, &shift);
                let s_stuff : String = format!("\\lstinputlisting[language=HTML, style=custom]{{{}}}", tempy);
                string = format!("{0}{1}", s_stuff, string);
                difference = tempy.len() as i32 + 4;
              },
              'x' => {
                //println!("matched!");
                let (tempy, exclude_this) = lst_input(w, txt, &shift);
                let s_stuff : String = format!("\\lstinputlisting[language=XML, style=custom]{{{}}}", tempy);
                string = format!("{0}{1}", s_stuff, string);
                difference = tempy.len() as i32 + 4;
              },
              _ => {
                println!("Could not identify language code block.")
              },
            }
          }
         else if zero  == '<'  && two == '>' {
            match one as char {
              'j' => {
                //println!("matched!");
                string = format!("{}{}{}{}{}{}", string, BEGIN, LISTING, "[language = Java]".to_string(), BEGIN, VERBATIM);
                difference = 3;
              },
              'p' => {
                //println!("matched!");
                string = format!("{}{}{}{}{}{}", string, BEGIN, LISTING, "[language = Python]".to_string(), BEGIN, VERBATIM);
                difference = 3;
              },
              'x' => {
                //println!("matched!");
                string = format!("{}{}{}{}{}{}", string, BEGIN, LISTING, "[language = XML]".to_string(), BEGIN, VERBATIM);
                difference = 3;
              },
              'r' => {
                //println!("matched!");
                string = format!("{}{}{}{}{}{}", string, BEGIN, LISTING, "[language = Rust]".to_string(), BEGIN, VERBATIM);
                difference = 3;
              },
              'h' => {
                //println!("matched!");
                string = format!("{}{}{}{}{}{}", string, BEGIN, LISTING, "[language = HTML]".to_string(), BEGIN, VERBATIM);
                difference = 3;
              },
              _ => {
                println!("Could not match language block for listing.");
              },
            }
          }
          else if zero == '<' && one == '/' && three == '>' {
            match two {
              _ => {string = format!("{}{}{}{}{}", string, END, VERBATIM, END, LISTING);
              difference = 3; },
            }
          }
         else if zero  == NUMBER && one   == NUMBER && two  == NUMBER && three  == NUMBER && four  == NUMBER && five  == NUMBER {
              //println!("matched!");
              string = format!("{1}{0}", "\\Huge ".to_string(), string);
              difference = 5;
          }
          else if zero  == NUMBER && one   == NUMBER && two  == NUMBER && three  == NUMBER && four  == NUMBER {
            //println!("matched!");
            string = format!("{1}{0}", "\\huge ".to_string(), string);
            difference = 4;
          }
          else if zero  == NUMBER && one   == NUMBER && two  == NUMBER && three  == NUMBER {
            //println!("matched!");
            string = format!("{1}{0}", "\\LARGE ".to_string(), string);
            difference = 3;
          }
          else if zero  == NUMBER && one   == NUMBER && two  == NUMBER {
            //println!("matched!");
            string = format!("{1}{0}", "\\Large ".to_string(), string);
            difference = 2;
          }
          else if zero  == NUMBER && one == NUMBER {
            string = format!("{1}{0}", "\\large ".to_string(), string);
            //println!("matched!");
            difference = 1;
          }
          else if zero  == NUMBER {
            string = format!("{1}{0}", "\\normalsize ".to_string(), string);
            //println!("matched!");
          }
          else {
            string = format!("{}{}", string, zero);
          }
          while difference >= 0 {
            zero = one;
            one = two;
            two = three;
            three = four;
            four = five;
            five = char_option(vectxt.next());
            //println!("{}{}{}{}{}{}", zero , one  , two , three , four , five);
            shift += 1;
            difference -= 1;
          }
        }
        write!(w, "{}", string);
        if boo {
          write!(w, "\\newline\n");
        }
        write!(w, "")
      },
      SmplTxt::Bold(ref txt) => write!(
          w, "{}{{{}}}",  BOLD_TXT, txt
      ),
      SmplTxt::Ital(ref txt) => write!(
          w, "{}{{{}}}",  ITAL_TXT, txt
      ),
      SmplTxt::Code(ref txt) => write!(
          w, "{}{{{}}}",  MONOSPACE, txt
      ),
      SmplTxt::Href(ref url, ref txt) => write!(
          w, "{}{{{}}}{{{}}}" , HREF, txt, url
      ),
    }
  }
}
/// To latex translation, with indentation.
pub trait ToLatexIndent {
  /// Translates something to latex, writes it in the writer.
  fn to_latex<Writer: Write>(& self, & mut Writer, & str, boo : bool) -> IoRes<()> ;
}
impl ToLatexIndent for RichTxt {
  #[allow(unused_imports)]
  fn to_latex<Writer: Write>(
    & self, w: & mut Writer, INDENT: & str, boo :bool
  ) -> IoRes<()> {
    // Bring all variants of `RichTxt` in scope.
    use lib_texdown::ast::RichTxt::* ;
    match * self {
      RichTxt::Code(ref language, ref code) => {
      //println!("@ RichTxt:: CODE!");
      //println!("indent {}", INDENT);
      for num in 0..code.len(){
        //println!("{}", code[num]);
      }
      write!(
        w, "{}{}{}\n{}\n{}{}\n",
        BEGIN, LISTING, FRAGILE,
        match * language {
          None => "",
          Some(ref l) => {
            //println!("language is {}", l);
            l as &str},
          }, LISTING,
          END
        )
      },
      RichTxt::Smpl(ref smpl) => {//println!("RichTxt::Smpl");
        //println!("indent {}", INDENT);
        ToLatex::to_latex(smpl, w, CHAR_SPACE, false)
        },
      RichTxt::Seq(ref seq) =>  { //println!("SEQUENCE");
      //println!("indent {}", INDENT);
        for n in 0..seq.len() {
          let ref x : RichTxt = seq[n];
          match x {
            & RichTxt::Smpl(ref x) => {
              ToLatex::to_latex(x, w, CHAR_SPACE, true);
              write!(w, "");
            },
            _ => {
              //println!("@not simple");
              to_latexrec(x, w, INDENT);
              write!(w, "");
             },
          }
        }
        write!(w, "\n")
      },
      RichTxt::Enum(ref enm) => { //println!("Enum");
        write!(w, "\\begin{{itemize}} ");
        if enm.len() == 0{
          //println!("enum is 0");
        }
        for n in 0..enm.len() {
          let ref x : RichTxt = enm[n];
          try!(write!(w, "\\item ") );
          match x {
            & RichTxt::Smpl(ref smpl) => {
              //write!(w, "\\item ");
              ToLatex::to_latex(smpl, w, CHAR_SPACE, false);
            },
            _ => {
              //write!(w, "\\item ");
              to_latexrec(x, w, INDENT);
            },
          }
        }
        try!(write!(w, "\\end{{itemize}} "));
        write!(w, "\n")
      },
      RichTxt::Quote(ref rich) => { //println!("Quote");
        //println!("indent {}", INDENT);
        to_latexrec(& rich, w, INDENT) ;
        write!(w, "")
      },
      RichTxt::NewLine => {
        //println!("indent {}", INDENT);
        write!(w, "\\NewLine\n")
      },
    }
  }
}
//For vectored enums (Richtxt only).
fn to_latexrec<Writer: Write>( x:
  & lib_texdown::ast::RichTxt, w : & mut Writer, INDENT: & str
) -> IoRes<()> {
  // Bring all variants of `RichTxt` in scope.
  use lib_texdown::ast::RichTxt::* ;
  match * x {
    RichTxt::Code(ref language, ref code) => { //succesfully converts to latex file
      //println!("@ RichTxt:: CODE!");
      for num in 0..code.len() {
        //println!("{}", code[num]);
    }
    write!(
      w, "{}{}\n{}{}\n{}\n",
      BEGIN, LISTING,
      match * language {
        None => "",
        Some(ref l) => {
          //println!("language is {}", l);
          l as &str},
      },
      END, LISTING
    )
  },
    RichTxt::Smpl(ref smpl) => { //println!("Smpl");
      ToLatex::to_latex(smpl, w, CHAR_SPACE, false)},
    RichTxt::Seq(ref seq) =>  {
      for n in 0..seq.len() {
          let ref x : RichTxt = seq[n];
          match x {
            & RichTxt::Smpl(ref x) => {
              ToLatex::to_latex(x, w, CHAR_SPACE, false);
              //write!(w, "");
            },
            _ => {
              //println!("@not simple");
              to_latexrec(x, w, INDENT);
              //write!(w, "");
             },
          }
      }
      write!(w, "")},
    RichTxt::Enum(ref enm) => { //println!("Enum");
        write!(w, "\\begin{{itemize}} ");
        for n in 0..enm.len() {
          try!(write!(w, "\\item ") );
          let ref x : RichTxt = enm[n];
          match x {
            & RichTxt::Smpl(ref smpl) => {
              //write!(w, "\\item ");
              ToLatex::to_latex(smpl, w, CHAR_SPACE, false);
            },
            _ => {
              //write!(w, "\\item ");
              to_latexrec(x, w, INDENT);
            },
          }
        }
        write!(w, "\\end{{itemize}} ");
        write!(w, "\n")
      },
    RichTxt::Quote(ref rich) => { //println!("Quote");
      //println!("indent {}", INDENT);
      to_latexrec(& rich, w, INDENT) ;
      write!(w, "") },
    RichTxt::NewLine => write!(w, "\\newLine"),
  }
}
//Intermediary Function
fn intermediary_fn<Writer: Write>(option : Option<lib_texdown::ast::RichTxt>, w : & mut Writer, INDENT : & str) -> IoRes<()>
{
  match option {
    Some(ref x) => to_latexrec(x, w, INDENT),
    None => write!(w, ""),
  }
}
// Implementing `ToLatexIndent` for `Frame` too.
impl ToLatex for Frame {
  fn to_latex<Writer: Write>(& self, w: & mut Writer, blah : &str, boo : bool) -> IoRes<()> {
    // Write start of frame, with indentation.
    try!( write!(w, "\\begin{{frame}}{{") ) ;
    // Write title (simple text).
    try!( self.title().to_latex(w, CHAR_SPACE, false) ) ;
    // Close title delimiter.
    try!( write!(w, "}}\n\n") ) ;
    // Write the body of the frame, indented by two  spaces.
    try!( self.body().to_latex(w, "  ", false) ) ;
    // Write END of frame.
    write!(w, "\n\\end{{frame}}\n\n\n")
  }
}
// Implementing `ToLatexIndent` for `Frames` too.
impl ToLatex for Frames {
  fn to_latex<Writer: Write>(& self, w: & mut Writer, blah: &str, boo : bool) -> IoRes<()> {
    // Write header, see below for the definition of this function.
    try!( write_header(w) ) ;
    // Start document.
    try!( write!(w, "\\begin{{document}}\n\\pgfplotsset{{compat=1.13}}\n") ) ;
    // Write all frames.
    for frame in self.get() {
      try!( frame.to_latex(w, CHAR_SPACE, false) )
    }
    // End document.
    write!(w, "\\end{{document}}\n\n")
  }
}
/// Allows to write something in latex to a file directly.
///
/// Builds on the `to_latex` function provided by `ToLatex`.
pub trait ToLatexFile: ToLatex {
  /// Writes something in latex to a file directly.
  fn to_latex_file(& self, target: & str) -> IoRes<()> {
    use std::fs::OpenOptions ;

    let mut file = try!(
      OpenOptions::new().create(true).truncate(
        true
      ).write(true).open(target)
    ) ;
    self.to_latex(& mut file, CHAR_SPACE, false)
  }
}
impl ToLatexFile for Frames {}
/// Writes the latex header for a presentation.
///
/// TODO: Add title and authors (and date?).
fn write_header<Writer: Write>(
  w: & mut Writer
) -> IoRes<()> {
  write!(w,
    r#"
\documentclass[10pt]{{beamer}}
\usetheme{{Warsaw}}

% |===| Package imports.

\usepackage{{
  etex, graphicx, amssymb, amsmath, amstext, amsfonts, mathtools,
  multicol, multirow, pgfplots, array, listings, colortbl, ulem, ifthen,
  xcolor, mathrsfs, xspace, rotating, soul
}}
\usepackage[scaled=1]{{beramono}}
\usepackage[T1]{{fontenc}}
\usepackage[utf8]{{inputenc}}
\usepackage{{tikz}}
\usepackage{{zlmtt}}

\usetikzlibrary{{shapes,arrows}}

\renewcommand{{\baselinestretch}}{{1.2}}

\hypersetup{{
  colorlinks=true,
  linkcolor=blue,
  urlcolor=red
}}

\lstdefinelanguage{{Rust}}
{{
  morestring=[b],
  morestring=[s]{{"}}{{"}},
  morecomment=[s]{{////}}{{//////}},
  stringstyle=\color{{black}},
  identifierstyle=\color{{darkblue}},
  keywordstyle=\color{{cyan}},
  morekeywords={{fn, let, while, do, if, else, for, crate, extern, pub,static, use, mod, usize, u32, isize, u16, i16, u8, i8, mut, ref, char, byte, char, Some, None, Ok, Err, return, impl, trait, struct, type}},
  backgroundcolor=\color{{white}},
  numbers=left,
  numberstyle=\tiny\ttfamily\color{{gray}},
  numbersep=.0001pt
}}
\lstdefinestyle{{custom}}{{
  belowcaptionskip=1\baselineskip,
  breaklines=true,
  xleftmargin=\parindent,
  showstringspaces=false,
  basicstyle=\tiny\ttfamily,
  keywordstyle=\bfseries\color{{blue!40!black}},
  commentstyle=\itshape\color{{purple!40!black}},
  identifierstyle=\color{{gray}},
  stringstyle=\color{{green}},
}}

"#
  )
}
fn matcher(vec : Vec<u8>, string : &str) -> () {
  to_file(&vec, string);
  let x  = String::from_utf8(vec);
  match x {
    Ok(s) => {
      println!("{}", s);
    },
    Err(s) => println!("Error converting utf8 bytes to string and LaTeX file!"),
  }
}
fn to_file(vec : & Vec<u8>, string : &str) -> Result<(), io::Error> {
  //println!("<--- Making file --->");
  let mut f = try!(File::create(string));
  try!(f.write(vec));
  Ok(())
}
/// CLAP key for output directory.
static output_key: & 'static str = "output_dir" ;
/// Default output directory.
static output_default: & 'static str = "output" ;
/// CLAP key for input file.
static input_key: & 'static str = "input_file" ;
/// CLAP key for verbosity.
static verbose_key: & 'static str = "VERB" ;

/// Extension for the files generated.
/// CHANGE THIS DEPENDING ON WHAT YOU GENERATE.
pub static extension: & 'static str = "tex" ;

fn try_io<T>(res: std::io::Result<T>, txt: String) -> T {
  match res {
    Ok(res) => res,
    Err(err) => {
      use std::process::exit ;
      println!("{}", txt) ;
      println!("> {}", err) ;
      exit(2)
    }
  }
}
fn main() {
  use std::path::Path ;
  use std::fs::DirBuilder ;
   let matches = {
    App::new("texdown to LaTeX").version(
      "0.1.0"
    ).author(
      "Adrien Champion <adrien.champion@email.com>, Shivam Batra <shivam-batra@uiowa.edu>, Daniel Apatiga <daniel-apatiga@uiowa.edu>"
    ).about(
      "Translates texdown files to LaTeX"
    ).arg(
      Arg::with_name(output_key).short("o").long("out").help(
        "Sets the output directory"
      ).takes_value(true)
    ).arg(
      Arg::with_name(input_key).help(
        "The input file"
      ).required(true).index(1)
    ).arg(
      Arg::with_name(verbose_key).short("v").help("Activates verbose output")
    ).get_matches()
  } ;

  // Retrieving output dir.
  let out_dir = matches.value_of(output_key).unwrap_or(output_default) ;

  // Retrieving input file.
  let source = {
    match matches.value_of(input_key) {
      Some(input) => input,
      None => unreachable!(),
    }
  } ;

  // Retrieving verbosity.
  let verbose = match matches.occurrences_of(verbose_key) {
    0 => false, _ => true,
  } ;

  if verbose {
    println!("")
  } ;

  // Create output directory if necessary.
  try_io(
    DirBuilder::new().recursive(true).create(& out_dir),
    format!("Error creating output directory \"{}\":", out_dir)
  ) ;

  // **Safely** building path to target file.
  {
    let source_path = Path::new(source) ;
    let mut target_path = Path::new(out_dir).to_path_buf() ;
    let file_name = match source_path.file_stem() {
      Some(file_name) => file_name,
      None => unreachable!(),
    } ;
    target_path.push(file_name) ;
    target_path.set_extension(extension) ;
    let target = target_path.as_path() ;

    let target = target.to_str().unwrap() ;

    if verbose {
      println!("|===| Translating to file \"{}\"", target) ;
    } ;
    let frames = generate_presentation(source);
    let mut string : Vec<u8> = Vec::new();
    ToLatex::to_latex(& frames, & mut string, CHAR_SPACE, false);
    matcher(string, target);

    if verbose {
      println!("| done") ;
      println!("|===|") ;
      println!("")
    }
  }
}
