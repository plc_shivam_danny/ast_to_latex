Ideas for a potential **Drone** business and How Drones are Related to Networking
  -Presentation and Proposal By Daniel Alexander Apatiga
    -for CS:3640 taught by Professor Behnam Dezfouli
    -The University of Iowa
`About them drones`  
  -Drones are expensive compared to how much they really *cost*.
    -a 1,500 dollar drone costs around 300 dollars to make.
    -could buy a 3D printer and hire an architect or someone good with CAD to design a chassis for it that is **water proof**.
      -drones are useful for many applications such as upping the value on houses for real-estate purposes.
  -The business would provide the software and drones themselves.
    -could be selfish and make the software be pirate resistant.  
    -or, could make it open source, 
    -write the code once and update it.
    -never again have to write code for this.
    -would enable you to work on other things.
    -basically, assembly is all you have to do after the code is written.
Hardware features of your **drone**
  -it would have an altimeter (measures altitude).
  -[GPS.]( https://www.adafruit.com/products/746?gclid=CM2_rcWGrcsCFZUjgQodO_kJ_Q)
  -An accelerometer sensor for measuring acceleration [for arduino.](http://www.arduinosensors.com/index.php/arduino-6dof-motion-sensor-the-mpu6050-inertial-measurement-unit/)  
  -A wi-fi adapter for remote control.  The wi-fi adapter would be connected to a raspberry pi usb port.
  -The wi-fi needs to have an omni-directional antennae on both the host-laptop and the client-drone.  See [figure](http://www.mobilemark.com/wp-content/uploads/2013/08/directional-terminology1.jpg) here for a quick, visual description.      
  -Alternatively, you could buy an [arduino](http://www.kickstarter.com/projects/flutterwireless/flutter-20-wireless-arduino-with-half-mile-1km-ran) wi-fi chip board.
    -except, you should have comparable wi-fi range on your laptop.  
      -do not have the range of one of your nodes look like [this access point's range](http://i.imgur.com/idwCGPq.png) compared with the other device. 
Hardware continued...
  -A barometer for sensing humidity in case it's close to raining [for arduino.](https://www.adafruit.com/products/746?gclid=CM2_rcWGrcsCFZUjgQodO_kJ_Q)
  -Infrared camera that can be turned on/off for flying at night.
  -A mount that can rotate for a high-def 4k video camera (sold separately). For first-person camera footage, buy a [camera](https://www.adafruit.com/products/1367)
Essential hardware continued...
  -The **drone** would have an Arduino and a Raspberry Pi.  
    -For documentation on how to accomplish this, click [here](https://www.raspberrypi.org/magpi-issues/MagPi07.pdf)  
  -The differences between them are the following:  An Arduino doesn't have as much processing power as a Raspberry Pi.  Nor does it have a *nice* **OS**, but an Arduino, in contrast, already has a lot of stable add-ons available from the **IoT** market.
What would the Raspberry pi do with the Arduino?
  -Information would be passed via pins from the Arduino to the Raspberry Pi or similar device.
    -[another website](http://www.instructables.com/id/The-Raspberry-Pi-Arduino-Connection/) that describes this.
    -The raspberry pi can power the Arduinos via the GPIO pins.
    -An Adruino could also be plugged in via USB if you want multiple Arduinos fetching data, according to [this website](https://oscarliang.com/connect-raspberry-pi-and-arduino-usb-cable/). 
  -Power supply would be different from what powers the motors.
    -Transistors would be used for controlling voltage flow to the motors.
      -you want motors with a very fast RPM
      -according to [this website](https://quadcopterproject.wordpress.com/static-thrust-calculation/), calculate the weight of the drone and then calculate the lift that can be created by each motor.
So, if your business makes drones, what will allow them to *fly*?
  -Without requiring a **network infrastructure** like what people use day-to-day, it's *possible* to set up a laptop with a unique, long-range wi-fi adapter that can act as a reliable method of communication with a drone as if it were **remote** *controlled*.
  -Can have the drone *connected* to the `3G network` in case it goes beyond wi-fi range.  Use a [GSM shield.](https://www.arduino.cc/en/Main/ArduinoGSMShield)
    -can have the cellular NIC switch turned on when wi-fi is out of range for saving power.  
    -has a maximum bandwidth of of 86.5kpbs, which is not much.
    -when the GSM shield is running, probably should not send much video through the network and you should turn off various features like sending accelerometer data for instance.
    -would cost 10 dollars for a gigabyte from most cellular ISPs.
Ad hoc will allow the drones to **fly**
  -in this particular infrastructure-less network, you can use multiple, simultaneous connections on different ports for various data and not have the unreliability of being attached to the internet that has queueing, propagation, and cumulative processing delays.  
    -fancy-shmancy non-required essentials
      -one port for altimeter readings (`UDP`); 
      -one for barometer (pressure) (`UDP`); 
      -one for *GPS* (`UDP`); 
    -required ports
      -one gyroscope port (`UDP`); 
      -one for accelerometer (`UDP`); 
      -one for the video feed (`TCP/IP`); 
      -one for controlling the flight (`UDP`); 
      -one for miscellaneous/error reporting (`TCP/IP`).
  -difficult part will be incorporating all of these ports into the program.
What else will the *software* be able to do?
  -The software will need a *video feed class* that can record while it receives the feed.  
    -records on the laptop and/or a USB dongle that is on the Raspberry Pi.
    -4k footage would be stored on the camera and not transmitted, unless it is heavily processed by the Raspberry Pi's quad processors.
  -On the raspberry pi that is in the drone, it will need to perform various tasks such as hovering and it will need some algorithms for controlled descending and landing that require no effort on the part of the user.
**Software continued...**
  -The software will also need a way of returning the drone to a location designated by the user via latitude and longitude coordinates that the GPS can understand.  
  -since `accelerometers` can measure speed with some calculus, knowing the ETA can be useful and an algorithm for that can be created.   
  -for controlling flight, do not use the `GPS` since it is not as accurate (D.Jones).
**Software** doubly continued...
  -On the smart phone, an app that can take over control of the drone should be possible in case the drone moves out of wi-fi range.  
  -The software on the drone should be able to recognize that the connection has been lost, so it connects instead to the cellular network, which will be helpful for long-range drone excursions.
    -would require joining a monthly plan for a gigabyte of data, typically 10 dollars.
**Software** triply continued…
  -Have these `Java classes` in an event-driven framework like from object-oriented programming class.  
    -Error reporting sender,
      -like all other senders in this list, it will send on a unique port.   
    -GPS coordinates sender, 
    -accelerometer data sender, 
    -signal quality sender,
      -in case the drone flies too far away from you, you will receive an error message saying fly it back. 
    -battery status sender, 
      -for knowing when to land it.
  -**Those would be on the drone**
**Software** *x4* continued...
  -altimeter sender,
   -for fun, not necessary. 
  -gyroscope sender, 
   -for relaying angular change in motion.  
   -not the best model, but here's an [example]( http://playground.arduino.cc/Main/Gyro)
  -distance sensor sender, 
   -can be bought for 2 dollars a piece.
   -should have more than four hooked up at the same time.
  -and finally video feed sender. 
   -wouldn't send the 4k camera footage,
   -would send raspberry pi camera footage.
On the *laptop*, some Java classes might be…
  -FlightCommandsToAST, 
  -ControllerSender, 
    -if written in Java, use [Jinput library](https://java.net/projects/jinput) or [LWJGL](https://www.youtube.com/watch?v=jwFzrBiH8X0)
  -Error, 
    -for reporting errors on the **host computer**.
  -ErrorClient, 
    -for receiving error messages from the **drone** and displaying them on the GUI.
The **laptop** `Java classes`, continued...
  -ProximitySensorClient, 
    -for receiving data on how far the drone is from objects so you don't fly into them. 
  -StartHost,
    -the main class that starts up everything. 
  -SignalHost
    -receives wi-fi signal quality data from the drone in percentage.
  -VidFeedInterpreter 
    -would interpret raw video footage.
    -a great [stackoverflow thread](http://stackoverflow.com/questions/2165593/how-to-decode-h-264-video-frame-in-java-environment) that mentions this.
  -GUI
    -would combine video footage, data readings from the drone into a nice, beautiful layout.
**Software** *Security* features...
  -Software will be written in *Java* mostly, and maybe some *Python* though the library for Python seems limited for what we need to do here.
    -For the *software* on the **host computer**, it could be written in Java, C++/C, Rust, etc.
  -You can have software security that prevents pirates from redistributing it. 
  -Or, you could have it as open-source.
  -Could just use [open-source software](https://3dr.com/software/) rather than write your own.
The **Java** software…
  -To simplify matters, the software should simplify the flight-control commands that are sent from the host laptop to the client drone by using a minimalist language. 
  -A single command can be one byte of data, or a single character, for example, “<” for turn left. The commands should be sent as UDP packets, in my opinion, too, so if they get lost they are lost forever and are not resent after some `‘x’` amount of time. 
    -**packet loss** shouldn't be a big problem in `ad hoc mode`.
      -though, it does still occur, *frequently*.
    -packet loss would be a big problem if using the drone in cellular network mode.
  -Reduces drone unreliability un-intuitively.
  -Should it have `pre-installed` **software**?
    -make it pre-installed, but have updates available for your clients in your business.
**Drone** main host `class`
  Some samples of my `code`
  <j=dronestart.java>
**SignalQuality** client-drone `class`
  <j=SignalSender.java>
**Error** `class`
  <j=error.java>
**Error** host `class`
  <j=errorhost.java>
**Minimalist** *language* to AST `class`
  <j=minimalist_to_ast.java>
Additional Sources:
  -“Building your own Drones: A beginner’s Guide to Drones, UAVs, and ROVs” by John Baichtal
  -“Exploring Arduino: Tools and Techniques for Engineering Wizardry” by Wiley
Thanks to:
  -Adrien C.,
  -Professor Nestan,
  -Shivam B., 
  -Professor D. Jones, and
  -Professor Behnam